Urban Data Visualization
========================

Setup Environment
~~~~~~~~~~~~~~~~~

::

        git clone https://gitlab.com/teamStark/udv-hack.git
        cd udv-hack
        pip install -r requirements.txt
        npm install -g gulp-cli
        npm install
        bower install


Prerequisites
~~~~~~~~~~~~~

-  Python 2.7 setup and installed
-  Pip setup and installed
-  Ensure all dependencies of requirements.txt are satisfied
-  Node, Gulp, Bower


Generate Documentation
~~~~~~~~~~~~~~~~~~~~~~

::

        # Generate HTML Documentation
        make html
        cd documentation/html && python -m SimpleHTTPServer
        [ Open browser to localhost:8000 for visualization ]

        # Generate Man Pages
        make man
        cd documentation/man && man -l t-vecs.1


        # Other Makefile options
        make

        Please use `make <target>' where <target> is one of
        html       to make standalone HTML files
        dirhtml    to make HTML files named index.html in directories
        singlehtml to make a single large HTML file
        pickle     to make pickle files
        json       to make JSON files
        htmlhelp   to make HTML files and a HTML help project
        qthelp     to make HTML files and a qthelp project
        applehelp  to make an Apple Help Book
        devhelp    to make HTML files and a Devhelp project
        epub       to make an epub
        epub3      to make an epub3
        latex      to make LaTeX files, you can set PAPER=a4 or PAPER=letter
        latexpdf   to make LaTeX files and run them through pdflatex
        latexpdfja to make LaTeX files and run them through platex/dvipdfmx
        text       to make text files
        man        to make manual pages
        texinfo    to make Texinfo files
        info       to make Texinfo files and run them through makeinfo
        gettext    to make PO message catalogs
        changes    to make an overview of all changed/added/deprecated items
        xml        to make Docutils-native XML files
        pseudoxml  to make pseudoxml-XML files for display purposes
        linkcheck  to check all external links for integrity
        doctest    to run all doctests embedded in the documentation (if enabled)
        coverage   to run coverage check of the documentation (if enabled)


Usage Details
~~~~~~~~~~~~~

::

        # Production ready code
        gulp build

        # Run the server, for API usage.

        python2 server.py

        # Development mode display

        gulp serve


Execution of Unit Tests
~~~~~~~~~~~~~~~~~~~~~~~

::

    # Mocha, Chai setup for unit testing

    gulp serve:tests
