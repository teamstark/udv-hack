FROM ubuntu:16.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y libpython3-dev python3-pip curl git \
    && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
    && apt-get install -y nodejs

WORKDIR /udv_hack
COPY requirements.txt package.json package-lock.json bower.json gulpfile.babel.js .babelrc .bowerrc /udv_hack/

RUN npm install -g gulp-cli bower \
    && npm install \
    && bower install --allow-root

RUN pip3 install --upgrade pip && \
    pip3 install -r requirements.txt

COPY redirect ./redirect
COPY data ./data
COPY app ./app

RUN gulp build

COPY server.py server.conf ./

EXPOSE 5000
CMD ["python3", "server.py"]
