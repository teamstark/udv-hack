CherryPy >= 3.6.0
scikit-learn >= 0.17.1
Sphinx >= 1.4
sphinx-rtd-theme >= 0.1.9
sphinxcontrib-seqdiag >= 0.8.5
numpy >= 1.11.0
openpyxl >= 2.3.5
scipy >= 0.18.1
configparser
