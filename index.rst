.. Urban Data Visualization documentation master file, created by
   sphinx-quickstart on Thu Apr  7 14:21:40 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UDV's documentation!
==================================

The main documentation for the site is organized into a couple sections:


.. _user-docs:

	.. toctree::
		:maxdepth: 2
		:caption: Usage Documentation

	   	README

.. _dev-docs:

	.. toctree::
		:maxdepth: 2
		:caption: Sequence Diagram for API Calls


	.. seqdiag::
		:align: center
   		:desctable:
   		:caption: Sequence Diagram for Firestation

		seqdiag {
			Client   ->  CherryPy [label = "GET Request /firestation"];
			CherryPy -> Openpyxl  [label = "Instantiate excel loader"];
			Openpyxl ->  firestation.xlsx [label = "Search for excel file"];
			firestation.xlsx  -> Openpyxl [label = "Load excel file"];
			Openpyxl -> CherryPy [label = "Preprocess Data"];
			CherryPy -> Client [label = "Response JSON"];
		    Client [description = "Browser"];
		    CherryPy [description = "Application Server"];
	        Openpyxl [description = "Excel loader & Parser"];
	        firestation.xlsx [description = "Excel firestation information"];
		}


	.. seqdiag::
		:align: center
   		:desctable:
   		:caption: Sequence Diagram for Bus Stop

		seqdiag {
			Client   ->  CherryPy [label = "GET Request /bus_stop"];
			CherryPy -> Openpyxl  [label = "Instantiate excel loader"];
			Openpyxl ->  bus_stop.xlsx [label = "Search for excel file"];
			bus_stop.xlsx  -> Openpyxl [label = "Load excel file"];
			Openpyxl -> CherryPy [label = "Preprocess Data"];
			CherryPy -> Client [label = "Response JSON"];
		    Client [description = "Browser"];
		    CherryPy [description = "Application Server"];
	        Openpyxl [description = "Excel loader & Parser"];
	        bus_stop.xlsx [description = "Excel Bus Stop information"];
		}


	.. seqdiag::
		:align: center
   		:desctable:
   		:caption: Sequence Diagram for Park

		seqdiag {
			Client   ->  CherryPy [label = "GET Request /park"];
			CherryPy -> Openpyxl  [label = "Instantiate excel loader"];
			Openpyxl ->  park.xlsx [label = "Search for excel file"];
			Openpyxl ->  ward.xlsx [label = "Search for excel file"];
			park.xlsx  -> Openpyxl [label = "Load excel file"];
			ward.xlsx  -> Openpyxl [label = "Load excel file"];
			Openpyxl -> CherryPy [label = "Preprocess Data & Combine Data"];
			CherryPy -> Client [label = "Response JSON"];
		    Client [description = "Browser"];
		    CherryPy [description = "Application Server"];
	        Openpyxl [description = "Excel loader & Parser"];
	        park.xlsx [description = "Excel Park information"];
	        ward.xlsx [description = "Ward information"]
		}


	.. seqdiag::
		:align: center
   		:desctable:
   		:caption: Sequence Diagram for Playground

		seqdiag {
			Client   ->  CherryPy [label = "GET Request /playground"];
			CherryPy -> Openpyxl  [label = "Instantiate excel loader"];
			Openpyxl ->  playground.xlsx [label = "Search for excel file"];
			playground.xlsx  -> Openpyxl [label = "Load excel file"];
			Openpyxl -> CherryPy [label = "Preprocess Data"];
			CherryPy -> Client [label = "Response JSON"];
		    Client [description = "Browser"];
		    CherryPy [description = "Application Server"];
	        Openpyxl [description = "Excel loader & Parser"];
	        playground.xlsx [description = "Excel Playground information"];
		}


	.. seqdiag::
		:align: center
   		:desctable:
   		:caption: Sequence Diagram for Waste Collection Center

		seqdiag {
			Client   ->  CherryPy [label = "GET Request /waste_collection_center"];
			CherryPy -> Openpyxl  [label = "Instantiate excel loader"];
			Openpyxl ->  waste_collection_center.xlsx [label = "Search for excel file"];
			waste_collection_center.xlsx  -> Openpyxl [label = "Load excel file"];
			Openpyxl -> CherryPy [label = "Preprocess Data"];
			CherryPy -> Client [label = "Response JSON"];
		    Client [description = "Browser"];
		    CherryPy [description = "Application Server"];
	        Openpyxl [description = "Excel loader & Parser"];
	        waste_collection_center.xlsx [description = "Excel Waste Collection Center information"];
		}


	.. seqdiag::
		:align: center
   		:desctable:
   		:caption: Sequence Diagram for Public Toilet Amenities

		seqdiag {
			Client   ->  CherryPy [label = "GET Request /public_toilet"];
			CherryPy -> Openpyxl  [label = "Instantiate excel loader"];
			Openpyxl ->  public_toilet.xlsx [label = "Search for excel file"];
			public_toilet.xlsx  -> Openpyxl [label = "Load excel file"];
			Openpyxl -> CherryPy [label = "Preprocess Data"];
			CherryPy -> Client [label = "Response JSON"];
		    Client [description = "Browser"];
		    CherryPy [description = "Application Server"];
	        Openpyxl [description = "Excel loader & Parser"];
	        public_toilet.xlsx [description = "Excel Public Toilet information"];
		}


	.. seqdiag::
		:align: center
   		:desctable:
   		:caption: Sequence Diagram for Public Complaints

		seqdiag {
			Client   ->  CherryPy [label = "GET Request /complaint"];
			CherryPy -> Openpyxl  [label = "Instantiate excel loader"];
			Openpyxl ->  complaint.xlsx [label = "Search for excel file"];
			complaint.xlsx  -> Openpyxl [label = "Load excel file"];
			Openpyxl -> CherryPy [label = "Preprocess Data"];
			CherryPy -> Client [label = "Response JSON"];
		    Client [description = "Browser"];
		    CherryPy [description = "Application Server"];
	        Openpyxl [description = "Excel loader & Parser"];
	        complaint.xlsx [description = "Excel Public Complaints information"];
		}


	.. seqdiag::
		:align: center
   		:desctable:
   		:caption: Sequence Diagram for Infer Similar Places

		seqdiag {
			Client   ->  CherryPy [label = "GET Request /infer_similar_places"];
			CherryPy -> ward_detailed_info  [label = "Instantiate excel loader"];
			ward_detailed_info ->  CherryPy [label = "Search for excel file"];
			CherryPy  -> AffinityPropogation [label = "Load excel file"];
			AffinityPropogation -> CherryPy [label = "Preprocess Data"];
			CherryPy -> Client [label = "Response JSON"];
		    Client [description = "Browser"];
		    CherryPy [description = "Application Server"];
	        AffinityPropogation [description = "Clustering from sklearn"];
	        ward_detailed_info [description = "Provide detailed ward information"];
		}


	.. seqdiag::
		:align: center
   		:desctable:
   		:caption: Sequence Diagram for Population

		seqdiag {
			Client   ->  CherryPy [label = "GET Request /population"];
			CherryPy -> Openpyxl  [label = "Instantiate excel loader"];
			Openpyxl ->  population.xlsx [label = "Search for excel file"];
			population.xlsx  -> Openpyxl [label = "Load excel file"];
			Openpyxl -> CherryPy [label = "Preprocess Data"];
			CherryPy -> Client [label = "Response JSON"];
		    Client [description = "Browser"];
		    CherryPy [description = "Application Server"];
	        Openpyxl [description = "Excel loader & Parser"];
	        population.xlsx [description = "Excel Population information"];
		}


	.. seqdiag::
		:align: center
   		:desctable:
   		:caption: Sequence Diagram for Population

		seqdiag {
			Client   ->  CherryPy [label = "GET Request /ward_detailed_info"];
			CherryPy ->  complaint  [label = "Request for Public Complaints Informaton"];
			CherryPy ->  public_toilet [label = "Request for Public Toilet Amenities Information"];
			CherryPy ->  park [label = "Request for Park Amenities Information"];
			CherryPy ->  population [label = "Request for ward specific Population Information"];
			CherryPy ->  waste_collection_center [label = "Request for waste collection center Information"];
			CherryPy ->  playground [label = "Request for Playground Amenities information"];
			population  -> CherryPy [label = "Information"];
			complaint  -> CherryPy [label = "Information"];
			park  -> CherryPy [label = "Information"];
			public_toilet  -> CherryPy [label = "Information"];
			waste_collection_center  -> CherryPy [label = "Information"];
			playground -> CherryPy [label = "Information"];
			CherryPy -> Client [label = "Response JSON"];
		    Client [description = "Browser"];
		    CherryPy [description = "Application Server"];
	        population [description = "Ward Specific Population Information"];
	        public_toilet [description = "Public Toilet Amenities Information"];
	        park [description = "Public Park Amenities Information"];
	        waste_collection_center [description = "Waste Collection Information"];
	        playground [description = "Public Playground amenities Information"];
	       	complaint [description = "Public Complaints Information"];
		}


Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
