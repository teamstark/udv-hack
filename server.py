#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""CherryPy Server to provide recommendations of semantic similarity."""

import os
import json
import collections
import itertools
import cherrypy
import configparser
from sklearn.cluster import AffinityPropagation
import numpy as np
from openpyxl import load_workbook


class Server(object):
    """
    Server Configuration for udv-hack.

    .. seealso::
        * :mod:`cherrypy`
    """

    def __init__(self):
        """Initialization of the server if needed."""
        pass

    @cherrypy.expose
    def ward_detailed_info(self):
        """Provide Detailed information / ward."""
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"

        def ensure_data_computed(filepath, process_func):
            if not os.path.exists(filepath):
                process_func()

        for filename, func in [
            ('complaint_op.json', self.complaint),
            ('public_toilet_op.json', self.public_toilet),
            ('park_op.json', self.park),
            ('population_op.json', self.population),
            ('waste_collection_center_op.json', self.waste_collection_center),
            ('playground_op.json', self.playground)
        ]:
            ensure_data_computed(
                filepath=os.path.join('data', filename),
                process_func=func
            )

        ward_data = json.load(
            open(os.path.join('data', 'wards.json'))
        )
        complaint_data = json.load(
            open(os.path.join('data', 'complaint_op.json'))
        )
        public_toilet_data = json.load(
            open(os.path.join('data', 'public_toilet_op.json'))
        )
        park_data = json.load(
            open(os.path.join('data', 'park_op.json'))
        )
        population_data = json.load(
            open(os.path.join('data', 'population_op.json'))
        )
        waste_collection_center_data = json.load(
            open(os.path.join('data', 'waste_collection_center_op.json'))
        )
        playground_data = json.load(
            open(os.path.join('data', 'playground_op.json'))
        )

        def merge_with_base(base_data, new_data, new_key, base_key):
            for new_item in new_data:
                try:
                    ward_id = int(new_item[new_key])
                except TypeError:
                    continue
                for base_item in base_data['features']:
                    try:
                        if int(base_item['properties'][
                            base_key
                        ]) == ward_id:
                            base_item['properties'].update(new_item)
                            break
                    except TypeError:
                        pass

        total_data = [
            complaint_data, public_toilet_data,
            park_data, population_data,
            waste_collection_center_data,
            playground_data
        ]
        for data in total_data:
            merge_with_base(
                base_data=ward_data,
                new_data=data['inside_bbmp_limits'],
                new_key='ward_id',
                base_key='WARD_NO',
            )
        return json.dumps(ward_data)

    @cherrypy.expose
    def population(self):
        """Provide ward specific population details."""
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        if not os.path.exists(os.path.join(
            'data', 'population_op.json'
        )):
            workbook = load_workbook(
                os.path.join('data', 'ward.xlsx'),
                read_only=True
            )
            sheets = workbook.get_sheet_names()
            inside_bbmp = self._extract_data_from_sheet(
                workbook=workbook,
                sheet_name=sheets[0],
                select_columns=[0, 1, 7, 8, 13],
                column_names=[
                    'ward_id', 'ward',
                    'ward_area', 'no_of_households', 'population_density'
                ]
            )
            payload = json.dumps({
                'inside_bbmp_limits': inside_bbmp,
                'outside_bbmp_limits': []
            })
            with open(os.path.join(
                'data', 'population_op.json'
            ), 'w') as f:
                f.write(payload)
            return payload
        else:
            payload = None
            with open(os.path.join(
                'data', 'population_op.json'
            ), 'r') as f:
                payload = f.read()
            return payload

    @cherrypy.expose
    def infer_similar_places(self):
        """Infer Similar Places."""
        data = json.loads(self.ward_detailed_info())
        features = [
            'complaint_count',
            'no_of_households',
            'park_count',
            'population_density',
            'playground_count',
            'waste_collection_center_count'
        ]
        arr = np.zeros(shape=(len(data['features']), len(features)))
        places_list = [
            item['properties']['ward_id'] for item in data['features']
        ]
        for i in range(len(data['features'])):
            item = data['features'][i]['properties']
            for j, feature in enumerate(features):
                arr[i][j] = item.get(feature, 0)
        t = arr.transpose()
        for i, record in enumerate(t):
            m = max(t[i])
            t[i] = [
                x / m for x in record
            ]
        arr = t.transpose()
        aff_cluster = AffinityPropagation()
        idx = aff_cluster.fit_predict(arr)
        k = [[] for _ in range(len(places_list))]
        word_centroid_map = dict(zip(places_list, idx))
        for word in word_centroid_map.keys():
            k[word_centroid_map[word]].append(word)
        k = [
            x for x in k if len(x) > 0
        ]

        def obtain_cluster_id(clusters, key):
            for index, cluster in enumerate(clusters):
                if key in cluster:
                    return index
        for i in range(len(data['features'])):
            item = data['features'][i]['properties']
            ward_id = item['ward_id']
            item['cluster_id'] = obtain_cluster_id(k, ward_id)
        return json.dumps(data)

    @cherrypy.expose
    def ward(self):
        """Provide basic ward details."""
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        data = None
        with open(os.path.join('data', 'wards.json'), 'r') as fp:
            data = fp.read()
        return data

    @cherrypy.expose
    def complaint(self):
        """Provide public complaints information."""
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        if not os.path.exists(os.path.join(
            'data', 'complaint_op.json'
        )):
            workbook = load_workbook(
                os.path.join('data', 'complaint.xlsx'),
                read_only=True
            )
            sheets = workbook.get_sheet_names()
            inside_bbmp = self._extract_data_from_sheet(
                workbook=workbook,
                sheet_name=sheets[0],
                select_columns=[2, 3, 7],
                column_names=['ward_id', 'ward', 'category']
            )
            data = {}
            for obj in inside_bbmp:
                try:
                    int(data[obj['ward_id']])
                    data[obj['ward']]['count'] += 1
                    data[obj['ward']]['category'].append(obj['category'])
                except TypeError:
                    pass
                except KeyError:
                    try:
                        int(obj['ward_id'])
                        data[obj['ward']] = {
                            'ward': obj['ward'],
                            'count': 1,
                            'ward_id': obj['ward_id'],
                            'category': [obj['category']]
                        }
                    except TypeError:
                        pass
            list_inside_bbmp = [
                {
                    'ward': val['ward'],
                    'ward_id': val['ward_id'],
                    'complaint_count': val['count'],
                    'complaint_category': collections.Counter(val['category'])
                } for key, val in data.iteritems()
            ]
            payload = json.dumps({
                'inside_bbmp_limits': list_inside_bbmp,
                'outside_bbmp_limits': []
            })
            with open(os.path.join(
                'data', 'complaint_op.json'
            ), 'w') as f:
                f.write(payload)
            return payload
        else:
            payload = None
            with open(os.path.join(
                'data', 'complaint_op.json'
            ), 'r') as f:
                payload = f.read()
            return payload

    @cherrypy.expose
    def public_toilet(self):
        """Provide public toilet Information."""
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        if not os.path.exists(os.path.join(
            'data', 'public_toilet_op.json'
        )):
            workbook = load_workbook(
                os.path.join('data', 'public_toilet.xlsx'),
                read_only=True
            )
            sheets = workbook.get_sheet_names()
            inside_bbmp = self._extract_data_from_sheet(
                workbook=workbook,
                sheet_name=sheets[0],
                select_columns=[1, 0],
                column_names=['ward', 'ward_id']
            )
            data = {}
            for obj in inside_bbmp:
                try:
                    int(data[obj['ward_id']])
                    data[obj['ward']]['count'] += 1
                except TypeError:
                    pass
                except KeyError:
                    try:
                        int(obj['ward_id'])
                        data[obj['ward']] = {
                            'ward': obj['ward'],
                            'count': 1,
                            'ward_id': obj['ward_id']
                        }
                    except TypeError:
                        pass
            list_inside_bbmp = [
                {
                    'ward': val['ward'],
                    'ward_id': val['ward_id'],
                    'public_toilet_count': val['count'],
                } for key, val in data.iteritems()
            ]
            payload = json.dumps({
                'inside_bbmp_limits': list_inside_bbmp,
                'outside_bbmp_limits': []
            })
            with open(os.path.join(
                'data', 'public_toilet_op.json'
            ), 'w') as f:
                f.write(payload)
            return payload
        else:
            payload = None
            with open(os.path.join(
                'data', 'public_toilet_op.json'
            ), 'r') as f:
                payload = f.read()
            return payload

    @cherrypy.expose
    def waste_collection_center(self):
        """Provide waste collection center Information."""
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        if not os.path.exists(os.path.join(
            'data', 'waste_collection_center_op.json'
        )):
            workbook = load_workbook(
                os.path.join('data', 'waste_collection_center.xlsx'),
                read_only=True
            )
            sheets = workbook.get_sheet_names()
            inside_bbmp = self._extract_data_from_sheet(
                workbook=workbook,
                sheet_name=sheets[0],
                select_columns=[5, 6],
                column_names=['ward_id', 'ward']
            )
            data = {}
            for obj in inside_bbmp:
                try:
                    int(data[obj['ward_id']])
                    data[obj['ward']]['count'] += 1
                except TypeError:
                    pass
                except KeyError:
                    try:
                        int(obj['ward_id'])
                        data[obj['ward']] = {
                            'ward': obj['ward'],
                            'count': 1,
                            'ward_id': obj['ward_id']
                        }
                    except TypeError:
                        pass
            list_inside_bbmp = [
                {
                    'ward': val['ward'],
                    'ward_id': val['ward_id'],
                    'waste_collection_center_count': val['count'],
                } for key, val in data.iteritems()
            ]
            payload = json.dumps({
                'inside_bbmp_limits': list_inside_bbmp,
                'outside_bbmp_limits': []
            })
            with open(os.path.join(
                'data', 'waste_collection_center_op.json'
            ), 'w') as f:
                f.write(payload)
            return payload
        else:
            payload = None
            with open(os.path.join(
                'data', 'waste_collection_center_op.json'
            ), 'r') as f:
                payload = f.read()
            return payload

    @cherrypy.expose
    def playground(self):
        """Provide playground Information."""
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        if not os.path.exists(os.path.join(
            'data', 'playground_op.json'
        )):
            workbook = load_workbook(
                os.path.join('data', 'playground.xlsx'),
                read_only=True
            )
            sheets = workbook.get_sheet_names()
            inside_bbmp = self._extract_data_from_sheet(
                workbook=workbook,
                sheet_name=sheets[0],
                select_columns=[1, 2, 6],
                column_names=['ward_id', 'ward', 'area']
            )
            data = {}
            for obj in inside_bbmp:
                try:
                    int(data[obj['ward_id']])
                    data[obj['ward']]['count'] += 1
                    data[obj['ward']]['area'] += obj['area']
                except TypeError:
                    pass
                except KeyError:
                    try:
                        int(obj['ward_id'])
                        data[obj['ward']] = {
                            'ward': obj['ward'],
                            'count': 1,
                            'ward_id': obj['ward_id'],
                            'area': obj['area']
                        }
                    except TypeError:
                        pass
            list_inside_bbmp = [
                {
                    'ward': val['ward'],
                    'ward_id': val['ward_id'],
                    'playground_count': val['count'],
                    'playground_area': val['area'] / 1000000
                } for key, val in data.iteritems()
            ]
            payload = json.dumps({
                'inside_bbmp_limits': list_inside_bbmp,
                'outside_bbmp_limits': []
            })
            with open(os.path.join(
                'data', 'playground_op.json'
            ), 'w') as f:
                f.write(payload)
            return payload
        else:
            payload = None
            with open(os.path.join(
                'data', 'playground_op.json'
            ), 'r') as f:
                payload = f.read()
            return payload

    @cherrypy.expose
    def park(self):
        """Provide park Information."""
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        if not os.path.exists(os.path.join(
            'data', 'park_op.json'
        )):
            workbook = load_workbook(
                os.path.join('data', 'park.xlsx'),
                read_only=True
            )
            sheets = workbook.get_sheet_names()
            inside_bbmp = self._extract_data_from_sheet(
                workbook=workbook,
                sheet_name=sheets[0],
                select_columns=[4, 6, 3],
                column_names=['ward', 'area', 'ward_id']
            )
            workbook_ward = load_workbook(
                os.path.join('data', 'ward.xlsx'),
                read_only=True
            )
            sheets = workbook_ward.get_sheet_names()
            ward_data = self._extract_data_from_sheet(
                workbook=workbook_ward,
                sheet_name=sheets[0],
                select_columns=[1, 10, 0],
                column_names=['ward', 'population', 'ward_id']
            )
            ward_specific_data = {}
            for ward in ward_data:
                ward_specific_data[ward['ward_id']] = {
                    'population': ward['population'],
                    'ward': ward['ward'],
                    'ward_id': ward['ward_id']
                }
            data = {}
            for obj in inside_bbmp:
                try:
                    int(data[obj['ward_id']])
                    data[obj['ward']]['area'] += obj['area']
                    data[obj['ward']]['count'] += 1
                except TypeError:
                    pass
                except KeyError:
                    try:
                        int(obj['ward_id'])
                        data[obj['ward']] = {
                            'area': obj['area'],
                            'ward': obj['ward'],
                            'count': 1,
                            'ward_id': obj['ward_id']
                        }
                    except TypeError:
                        pass
            list_inside_bbmp = [
                {
                    'ward': val['ward'],
                    'ward_id': val['ward_id'],
                    'park_count': val['count'],
                    'park_area': val['area'] * 0.00404686,
                    'park_area_per_population': (
                        val['area'] * 0.00404686 / ward_specific_data[
                            val['ward_id']
                        ]['population']
                    )
                } for key, val in data.iteritems()
            ]
            payload = json.dumps({
                'inside_bbmp_limits': list_inside_bbmp,
                'outside_bbmp_limits': []
            })
            with open(os.path.join(
                'data', 'park_op.json'
            ), 'w') as f:
                f.write(payload)
            return payload
        else:
            payload = None
            with open(os.path.join(
                'data', 'park_op.json'
            ), 'r') as f:
                payload = f.read()
            return payload

    @cherrypy.expose
    def bus_stop(self):
        """Provide bus stop Information."""
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        if not os.path.exists(os.path.join(
            'data', 'bus_stop_op.json'
        )):
            workbook = load_workbook(
                os.path.join('data', 'bus_stop.xlsx'),
                read_only=True
            )
            sheets = workbook.get_sheet_names()
            inside_bbmp = self._extract_data_from_sheet(
                workbook=workbook,
                sheet_name=sheets[0],
                select_columns=[3, 5, 6],
                column_names=[
                    'ward', 'bus_stop_latitude', 'bus_stop_longitude'
                ]
            )
            outside_bbmp = self._extract_data_from_sheet(
                workbook=workbook,
                sheet_name=sheets[1],
                select_columns=[0, 3, 4],
                column_names=[
                    'ward', 'bus_stop_latitude', 'bus_stop_longitude'
                ]
            )
            payload = json.dumps({
                'inside_bbmp_limits': inside_bbmp,
                'outside_bbmp_limits': outside_bbmp
            })
            with open(os.path.join(
                'data', 'bus_stop_op.json'
            ), 'w') as f:
                f.write(payload)
            return payload
        else:
            payload = None
            with open(os.path.join(
                'data', 'bus_stop_op.json'
            ), 'r') as f:
                payload = f.read()
            return payload

    @cherrypy.expose
    def firestation(self):
        """Provide Firestation Information."""
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        if not os.path.exists(os.path.join(
            'data', 'firestation_op.json'
        )):
            workbook = load_workbook(
                os.path.join('data', 'firestation.xlsx'),
                read_only=True
            )
            sheets = workbook.get_sheet_names()
            inside_bbmp = self._extract_data_from_sheet(
                workbook=workbook,
                sheet_name=sheets[0],
                select_columns=[3, 4],
                column_names=[
                    'firestation_latitude', 'firestation_longitude'
                ]
            )
            payload = json.dumps({
                'inside_bbmp_limits': inside_bbmp,
                'outside_bbmp_limits': []
            })
            with open(os.path.join(
                'data', 'firestation_op.json'
            ), 'w') as f:
                f.write(payload)
            return payload
        else:
            payload = None
            with open(os.path.join(
                'data', 'firestation_op.json'
            ), 'r') as f:
                payload = f.read()
            return payload

    def _extract_data_from_sheet(
        self, workbook, sheet_name, select_columns=[], column_names=[]
    ):
        """Extract column data from workbook in the specified sheet."""
        ws = workbook[sheet_name]
        columns = [
            ws.columns[index] for index in select_columns
        ]
        skip_header = [
            column[1:]
            for column in columns
        ]
        data = []
        for column in skip_header:
            column_data = []
            for cell in column:
                column_data.append(cell.value)
            data.append(column_data)
        obj_data = []
        column_names_cycle = itertools.cycle(column_names)
        for tup in zip(*data):
            obj = {}
            for val in tup:
                obj[next(column_names_cycle)] = val
            obj_data.append(obj)
        return obj_data

if __name__ == '__main__':
    """Setting up the Server with Specified Configuration"""

    server_config = configparser.RawConfigParser()
    conf = {
        '/': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.abspath(os.path.join(
               '.', 'dist'
            )),
            'tools.staticdir.index': 'index.html'
        }
    }
    server_config.read('server.conf')
    server_port = server_config.get('Server', 'port')
    server_host = server_config.get('Server', 'host')
    thread_pool = server_config.get('Server', 'thread_pool')
    queue_size = server_config.get('Server', 'queue_size')
    cherrypy.config.update({'server.socket_host': server_host})
    cherrypy.config.update({'server.thread_pool': int(thread_pool)})
    cherrypy.config.update({'server.socket_queue_size': int(queue_size)})
    cherrypy.config.update({'server.socket_port': int(
        os.environ.get('PORT', server_port)
    )})
    cherrypy.quickstart(Server(), '/', conf)
